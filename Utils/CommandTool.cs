using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime;
using System.Reflection;
using System.IO;


using WIODatasetLanguage;
using WIODataLanguage;

using DFlow;
using DFlowCore;

using MongoDB.Bson;

namespace DFlowCore
{

	//Usage: dflow -d <dataset_parse_expr> -m <file with model bson> -o <output path>
	//Besides it will look for DFLOW_ASSEMBLY_PATH and DFLOW_DATASET_PATH in env, for loading
	//Assemblies and dataset
	//TODO: plugins
	public class CommandTool{
		public Dictionary<string, string> parameters_dict  = new Dictionary<string,string>();
		public Dictionary<string, string> arguments = new Dictionary<string,string>();
		public Dictionary<string, string> help_dict = new Dictionary<string, string>(); //todo: fill up
		
		
		public CommandTool(){
			parameters_dict["-d"] = "dataset_expr";
			parameters_dict["-m"] = "model_expr_source";
			parameters_dict["-t"] = "training_dataset";
			parameters_dict["-h"] = "help";
			
			help_dict["-d"] = "Dataset expression : WIODatasetLanguage expression for the main" +
				"dataset we are interested in ";
			help_dict["-m"] = "Model expression source : path to file with json representation of" +
				"dflow model";
			help_dict["-t"] = "Training dataset : if the dataflow is in RequiredTraining state this parameters" +
				"HAS TO BE set";
		}

		public string DumpHelp(){
			string tmp = "";
			foreach(var x in help_dict.Keys)
				tmp+= x.ToString() +":" + help_dict[x].ToString();
			return tmp;
		}
		
		public void Load(string[] args){
			for(int i=0;i<args.Length;++i)
				if(parameters_dict.ContainsKey(args[i]))
					if(i!=args.Length -1 ) arguments[parameters_dict[args[i]]] = args[i+1];	
			else arguments[parameters_dict[args[i]]] = "";
		
			//help requested
			if(arguments.ContainsKey("help")){
				Console.WriteLine(this.DumpHelp());
				return;
			}
			
			
			Console.WriteLine("DFLOWCMDTOOL:{0}:{1}", 
			                  arguments["dataset_expr"], 
			                  arguments["model_expr_source"]);
			
			
			Console.WriteLine("DFLOWCMDTOOL:DFLOW_DATASET_PATH={0}",Environment.GetEnvironmentVariable("DFLOW_DATASET_PATH"));
	
//			foreach(string path in Environment.GetEnvironmentVariable("DFLOW_DATASET_PATH").Split (':')){
//				Console.WriteLine("DFLOWCMDTOOL:Loading {0} datasets",path);
//				WIODatasetLanguageEnv.Instance.RecursivelyLoadAssemblies(path);
//			}
	
			
			Console.WriteLine("DFLOWCMDTOOL:Loaded assemblies");
			
			//Load assemblies specified as dflow plugins
//			foreach(string path in Environment.GetEnvironmentVariable("DFLOW_PATH").Split (':'))
//				_RecursivelyLoadAssemblies(path);
				
			//Look for new assemblies loaded
			DFlowCore.DFlowEngineGlobalRegister.DoLookForNewAssemblies();
			
			Console.WriteLine("DFLOWCMDTOOL:Loaded plugins");
			
			Console.WriteLine("DFLOWCMDTOOL:Loading the main dataset");
			IDataset source_dataset = (IDataset)WIODatasetLanguageEnv.Instance.EvaluateDirectly(
				this.arguments["dataset_expr"]);
			Console.WriteLine("DFLOWCMDTOOL:Loaded the main dataset");
			IDataset training_dataset = null;
			
			if(arguments.ContainsKey("training_dataset")){
				Console.WriteLine("DFLOWCMDTOOL:Loading the training dataset");
				training_dataset = (IDataset)WIODatasetLanguageEnv.Instance.EvaluateDirectly(
				this.arguments["training_dataset"]);
				Console.WriteLine("DFLOWCMDTOOL:Loaded the training dataset");
			}
			
			
			string bson_command = File.ReadAllLines(this.arguments["model_expr_source"])[0];
			
			Dataflow master_dataflow = new Dataflow();
			master_dataflow.Load (
				MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(bson_command));

			
			//Omitted for now : LoadModel needs type and it is a little cumbersome
			//Here only for testing 
			//Not correct *LoadModel(object)*!, I think LoadModel should itself find type of
			//persistent data
			//Another problem is that BsonDocument doesn't allow for persistent data > 4mb, which
			//doesnt sound good
			if(this.arguments.ContainsKey("try_load")){
				Console.WriteLine("DFLOWCMDTOOL: Loading persistent data");
				int cnt = 0;
				foreach(var x in master_dataflow.GetNodes()){
					if(x is PersistentNode) ((PersistentNode)x).LoadModel(typeof(object));
					++cnt;
				}
			}
			
		
			
			List<List<object>> output = new List<List<object>>();
			
			
			if(master_dataflow.State == Dataflow.DataflowState.RequiresTraining){
				//train it
				if(training_dataset == null) throw new Exception("Not provided training dataset");
				master_dataflow.source_dataset = training_dataset;
				
				while(master_dataflow.State == Dataflow.DataflowState.RequiresTraining){
					foreach(var key in training_dataset.Keys()){

						master_dataflow.data_address = key;
						master_dataflow.Process();
						
					}
				}
			}
			
			if(master_dataflow.State != Dataflow.DataflowState.Ready){
				throw new Exception("Not ready dataflow. Error in training , or not corrected" +
				                    "model provided");
			}
			
			
			
			DFlow.Node LastNode = null;
			foreach(var node in master_dataflow.GetNodes()) LastNode = node;
			
			//Note : there is a problem with trainability : if it is a trainable node
			//First item just won't fire.. I can check it here or change trainable
			Console.WriteLine("DFLOWCMDTOOL:Master dataflow created");
			Console.WriteLine("DFLOWCMDTOOL:Running dataflow on the provided dataset..");
			master_dataflow.source_dataset = source_dataset;	              
			foreach(var k in source_dataset.Keys()){
				master_dataflow.data_address = k;
				master_dataflow.Process();
			}
			Console.WriteLine("DFLOWCMDTOOL:Running dataflow success");
			
			
			
			
			
			Console.WriteLine("DFLOWCMDTOOL:Finished");
		}
		

		private void _RecursivelyLoadAssemblies(string path){
			if(path.EndsWith(".dll")){
				Assembly.LoadFrom(path);
				return;
			}
			
			
			foreach(string file in Directory.GetFiles (path)){
				if(file.EndsWith(".dll"))
					Assembly.LoadFrom(file);
			}			
			foreach(string directory in Directory.GetDirectories(path))
				_RecursivelyLoadAssemblies(directory);
			
		}		
		
	}


}
