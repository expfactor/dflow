using System;
using System.Runtime.InteropServices;
namespace WIO.Utils
{
	public static class UtilityFunctions
	{
        public static T Cast<T>(dynamic o)
        {
            return (T)o;
        }
		
	    [DllImport("kernel32.dll", EntryPoint = "CopyMemory", SetLastError = false)]
	    public static extern void CopyMemory(IntPtr dest, IntPtr src, uint count);
	}
}

