using System;
using System.Runtime.InteropServices;
using System.Reflection;

using NUnit.Framework;
namespace WIO.Utils
{
	public unsafe class FixedArray<T> : IDisposable where T : struct
	{
	    public IntPtr arrayPtr = IntPtr.Zero;
		int len;
		public int Length{
			get{ return this.len; }
		}
	    public int sizeofT { get { return sizeof(T); } }
	
		
		public T[] ToArray(){
			T[] ret = new T[len];
			for(int i=0;i<len;++i) ret[i] =this[i];
			return ret;
		}		
		public K[] CastToArray<K>() where K: struct{
			K[] ret = new K[len];
			MethodInfo castMethod = typeof(WIO.Utils.UtilityFunctions).GetMethod("Cast").MakeGenericMethod(typeof(K));
			for(int i=0;i<len;++i) ret[i] = (K)castMethod.Invoke(null, new object[] { this[i] });
			return ret;
		}
		
	    public unsafe T this[int i]
	    {
	        get
	        {
				return ((T*)arrayPtr)[i];
	        }
	        set
	        {
				((T*)arrayPtr)[i] = value;
	        }
	    }
		public void Rewrite(float[] source){
			Assert.AreEqual(source.Length, this.Length);
			System.Runtime.InteropServices.Marshal.Copy (source,0,this.arrayPtr,this.Length);
		}
		public FixedArray(IntPtr arrayPtr, int len){
			this.arrayPtr = arrayPtr;
			this.len = len;
		}
	    public FixedArray(int length)
	    {
			this.len = length;
	        arrayPtr = Marshal.AllocHGlobal(sizeofT * length);
	    }
		public unsafe FixedArray(float[] source){
			this.arrayPtr = Marshal.AllocHGlobal(sizeofT * source.Length);
			System.Runtime.InteropServices.Marshal.Copy (source,0,this.arrayPtr, source.Length);
			this.len = source.Length;
		}
		
		private bool _disposed = false;
		public void Dispose(){
			if(this._disposed) return;
			Marshal.FreeHGlobal(this.arrayPtr);
			_disposed = true;
		}
		
		
		
	}
}

