﻿/*
 * DATAFLOWCORE
 * 
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace DFlow
{

    public class PinInfo
    {
        
        protected object o;
        protected System.Reflection.FieldInfo fi;

        public PinInfo(System.Reflection.FieldInfo _fi, object _o) { fi = _fi; o = _o; }
        public void SetValue(object v) { fi.SetValue(o, v); }
        public object GetValue() { return fi.GetValue(o); }
        public Type FiedType { get {

				return fi.FieldType; 
			}
		}
        public string Name { get { return fi.Name; } }
    }
}
