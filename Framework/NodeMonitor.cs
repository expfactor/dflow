﻿/*
 * DATAFLOWCORE
 * 
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlow
{
    public class NodeMonitor
    {
        public string name;
        public bool enabled = true;

        // on change ready state ?
        public virtual void OnAddWire(DFlow.Wire w) { }
        public virtual void OnRemoveWire(DFlow.Wire w) { }
        public virtual void OnPreProcess() { }
        public virtual void OnPostProcess() { }
    }
}
