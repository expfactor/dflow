﻿/*
 * DATAFLOWCORE
 * 
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlowCore
{
    public static class Random
    {
        static System.Random r = new System.Random();
        public static int  Range(int a, int b)
        {
            return a+(int)Math.Floor( r.NextDouble() * (b - (a + double.Epsilon)));
        }

        public static double Range(double a, double b)
        {
            return r.NextDouble() * (b - (a + double.Epsilon));
        }

        public static float Range(float a, float b)
        {
            return ((float)r.NextDouble()) * (b - (a + float.Epsilon));
        }

    }
}
