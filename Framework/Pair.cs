﻿/*
 * DATAFLOWCORE
 * 
Copyright 2013 - WIDE IO Limited
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DFlow
{
    public class Pair<U, V>
    {
        public U first;
        public V second;
        public Pair() { }
		public Pair(Pair<U,V> p) { first = p.first; second = p.second; }
        public Pair(U u, V v) { first = u; second = v; }
        public bool /*override*/ Equals(Pair<U, V> a, Pair<U, V> b)
        {
            return ((a.first.Equals(b.first)) && (a.second.Equals(b.second)));
        }

    }
}

//   public class EquatablePair<U, V> : Pair<U, V> { }
/*
 public class EquatablePair<U,V> : IEquatable< EquatablePair<U,V> > 
      where U : IEquatable<U> where V : IEquatable<V> {
      public U first;
      public V second;
      public EquatablePair() { }
      public EquatablePair(U u, V v) { first = u; second = v; }
      public bool Equals (EquatablePair<U,V> a, EquatablePair<U,V> b) { 
          return ((a.first.Equals(b.first))&&( a.second.Equals(b.second)));
      }          
  }
*/

