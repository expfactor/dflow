﻿/*
 * DATAFLOWCORE
 * 
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlowCore
{
    class DataInterfaces
    {

        // IVector : i.e. must be behave like a vector and be assignable to a k-dimensional continuous value
        // IDistance : i.e. a distance must be provided in-between the elements
        // IAlgebra : implement inner-product, add, 


        public interface IVector<T>
        {
             int GetNDim();
             T GetCoord(int i);
             void SetCoord(int i,T v);
        }

        public interface IDistanceWith<T>
        {
            float DistanceTo(T alt);
        }

        public interface ISimilarityWith<T>
        {
            // i.e. inner product
            float SimilarityTo(T alt);
        } 

    }
}
